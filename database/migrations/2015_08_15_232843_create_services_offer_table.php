<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_offer', function (Blueprint $table) {
            $table->increments('service_offer_id');
            $table->integer('service_id')->unsigned();
            $table->integer('branch_id')->unsigned();
            $table->string('status', 16)->default('active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('services_offer');
    }
}
