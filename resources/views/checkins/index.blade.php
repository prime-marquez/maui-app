@extends('layout')

@section('content')

    @include('_partial.header')
    
    @include('_partial.sidebar_left')

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            @if (Session::has('flash_message'))
            <div class="alert alert-success alert-block fade in">
                <button type="button" class="close close-sm" data-dismiss="alert">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="fa fa-ok-sign"></i>
                    Success!
                </h4>
                <p>{{ Session::get('flash_message') }}</p>
            </div>
            @endif

            <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Check Ins
                          </header>
                          <div class="panel-body">
                              <section id="unseen">
                                <table class="table table-bordered table-striped table-condensed">
                                  <thead>
                                  <tr>
                                      <th>ID</th>
                                      <th>Name</th>
                                      <th>Email</th>
                                      <th>Guest Contact No.</th>
                                      <th>Guests</th>
                                      <th>Service</th>
                                      <th>Price</th>
                                      <th>Venue</th>
                                      <th>Landmark</th>
                                      <th>Store Contact No.</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                 @foreach($checkins as $checkin)
                                  <tr>
                                      <td>{{$checkin->check_in_id}}</td>
                                      <td>{{$checkin->name}}</td>
                                      <td>{{$checkin->email}}</td>
                                      <td>{{$checkin->phone_no}}</td>
                                      <td>{{$checkin->no_of_guest}}</td>
                                      <td>{{$checkin->sv_name}}</td>
                                      <td>{{$checkin->price}}</td>
                                      <td>{{$checkin->address}}</td>
                                      <td>{{$checkin->landmark}}</td>
                                      <td>{{$checkin->telephone_number}}</td>
                                  </tr>
                                  @endforeach
                                  </tbody>
                              </table>
                              </section>
                          </div>
                      </section>
                      <div class="container">
                        <?php echo $checkins->render(); ?>
                  </div>
              </div>
        </section>
    </section>
    <!--main content end-->

    @include('_partial.slidebar_right')

    @include('_partial.footer')
@endsection
