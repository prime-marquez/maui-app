@extends('layout')

@section('content')

    @include('_partial.header')
    
    @include('_partial.sidebar_left')

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            @if (Session::has('flash_message'))
            <div class="alert alert-success alert-block fade in">
                <button type="button" class="close close-sm" data-dismiss="alert">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="fa fa-ok-sign"></i>
                    Success!
                </h4>
                <p>{{ Session::get('flash_message') }}</p>
            </div>
            @endif

            <div class="row">
                <div class="col-lg-12">
                    <!--breadcrumbs start -->
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="#">Library</a></li>
                        <li class="active">Data</li>
                    </ul>
                    <!--breadcrumbs end -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <section class="panel">
                        <header class="panel-heading">
                           Create A Branch
                        </header>
                        <div class="panel-body">

                            @if (count($errors) > 0)
                            <div class="alert alert-block alert-danger fade in">
                                <button type="button" class="close close-sm" data-dismiss="alert">
                                  <i class="fa fa-times"></i>
                                </button>
                                <strong>Whoops!</strong> There were some problems with your input.
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            <form class="form-horizontal" role="form" action="{{ url() }}/branch/store" method="post">
                                {!! csrf_field() !!}
                                
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Store Id</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="Store Id" class="form-control" name="store_id" value="{{ old('store_id')}}">
                                        <p class="help-block">Example block-level help text here.</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Address</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="Address" class="form-control" name="address" value="{{ old('address') }}">
                                        <p class="help-block">Address.</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Landmark</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="Landmark" class="form-control" name="landmark" value="{{ old('landmark') }}">
                                        <p class="help-block">Example block-level help text here.</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Store Hours</label>
                                    <div class="col-lg-10">
                                        <input type="type" placeholder="Store Hours" class="form-control" name="store_hours" value="{{ old('store_hours')}}">
                                        <p class="help-block">Example block-level help text here.</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Telephone Number</label>
                                    <div class="col-lg-10">
                                        <input type="text" placeholder="Telephone Number" class="form-control" name="telephone_number" value="{{ old('telephone_number') }}">
                                        <p class="help-block">Example block-level help text here.</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button type="submit" class="btn btn-info">Post</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
    <!--main content end-->

    @include('_partial.slidebar_right')

    @include('_partial.footer')
@endsection
