    <!--sidebar start-->
    <aside>
        <div id="sidebar"  class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a class ="{{ Request::is( 'dashboard') ? 'active' : 'null' }}" href="{{ url() }}/dashboard" >
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="{{ url() }}/users" class ="{{ Request::is('users') ? 'active' : 'null' }}">
                        <i class="fa fa-users"></i>
                        <span>Users</span>
                    </a>
                    <ul class="sub">
                        <li><a href="{{ url() }}/users/role/admin">Admin</a></li>
                        <li><a href="{{ url() }}/users/role/store_user">Store Owner</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="{{ url() }}/branches" class ="{{ Request::is('branch') ? 'active' : 'null' }}">
                        <i class="fa fa-laptop"></i>
                        <span>Branches</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="{{ url() }}/check-ins" class ="{{ Request::is('check-ins') ? 'active' : 'null' }}">
                        <i class="fa fa-book"></i>
                        <span>Check Ins</span>
                    </a>
                </li>
            </ul>
            <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->