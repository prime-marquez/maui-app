<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
   
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'services';
    

    /**
     * This a primary key
     *
     * @var string
     */
    protected $primaryKey = 'service_id';

   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'price', 'status'];


    public function branches()
    {

        return $this->hasManyThrough('App\ServicesOffer', 'App\Branches', 'service_id', 'branch_id');
    }
    
}